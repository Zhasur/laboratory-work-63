import React from 'react';
import './Contacts.css'
import Header from "../Header/Header";

const Contacts = () => {
    return (
        <div className="main-root">
            <Header/>
            <div className='contacts'>
                <form>
                    <h4>Contact form</h4>
                    <div className="contact">
                        <label htmlFor="name">Name <i className='star'>*</i></label>
                        <input type="text" name="name" id="name" className="enter-password" />
                        <label htmlFor="lastName">Last name <i className='star'>*</i> </label>
                        <input type="text" name="lastName" id="lastName" className="enter-password" />
                        <label htmlFor="number">Contact number <i className='star'>*</i></label>
                        <input type="text" name="number" id="number" className="enter-password" />
                        <label>Message</label>
                        <input type="text" className='message'/>
                        <button>Send</button>
                    </div>
                    <div className="contact-center">
                        <p>
                            При возникновении проблем в работе Контактной форме, а также для уточнения всех интересующих Вас
                            вопросов обращайтесь в «Контактный центр» по
                            телефону <b>8-800-555-0-555</b> или <b>8-495-981-981-9</b>.
                        </p>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default Contacts;